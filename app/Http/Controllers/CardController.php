<?php

    namespace App\Http\Controllers;

    use App\Models\Card;
    use Illuminate\Http\RedirectResponse;

    class CardController extends Controller
    {

        public function show(Card $card): Card {
                $card->load([
                    'card.users'
                ]);
                return $card;
        }

        public function store()
        {
            request()->validate([
                'board_id' => ['required', 'exists:boards,id'],
                'card_list_id' => ['required', 'exists:card_lists,id'],
                'title' => ['required'],
            ]);

            Card::create([
                'user_id' => auth()->id(),
                'board_id' => request('board_id'),
                'card_list_id' => request('card_list_id'),
                'title' => request('title'),
            ]);
        }

        public function update(Card $card): RedirectResponse
        {
            request()?->validate([
                'title' => ['required'],
                'description'
            ]);

            $title = request()?->string('title')->trim();
            $description = request()?->string('description')->trim();

            $updateArray = [
                'title' => $title ? $title : null,
                'description' => $description ? $description : null,
            ];

            $card->update($updateArray);

            return redirect()->back();
        }

        public function move(Card $card): RedirectResponse
        {
            request()->validate([
                'cardListId' => ['required', 'exists:card_lists,id'],
                'position' => ['required', 'numeric'],
            ]);

            $card->update([
                'card_list_id' => request('cardListId'),
                'position' => round(request('position'), 5),
            ]);
            return redirect()->back();
        }
    }
