<?php

	namespace App\Http\Controllers;

	use App\Models\Board;
	use App\Models\CardList;
	use Illuminate\Http\RedirectResponse;

	class CardListController extends Controller
	{
		public function store(Board $board): RedirectResponse
		{
			request()->validate([
				'listName' => ['required'],
			]);

			CardList::create([
				'board_id' => $board->id,
				'user_id' => auth()->id(),
				'name' => request('listName')
			]);

			return redirect()->back();
		}
	}
