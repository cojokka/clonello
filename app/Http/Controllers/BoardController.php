<?php

    namespace App\Http\Controllers;

    use App\Models\Board;
    use Illuminate\Http\RedirectResponse;
    use Inertia\Inertia;
    use Inertia\Response;

    class BoardController extends Controller
    {
        public function index(): Response
        {
            return Inertia::render('Boards/Index', [
                'boards' => auth()->user()->boards,
            ]);
        }

        public function show(Board $board): Response
        {
            $board->load([
                'user',
                'lists.cards' => fn($query) => $query->orderBy('position'),
                'lists.cards.user',
                'lists.user',
            ]);
            return Inertia::render('Boards/Show', [
                'board' => $board
            ]);
        }

        public function update(Board $board): RedirectResponse
        {
            request()->validate([
                'name' => ['required', 'max:255']
            ]);
            $board->update(['name' => request('name')]);

            return redirect()->back();
        }

        public function store(): void
        {
            request()->validate([
                'boardName' => ['required']
            ]);

            Board::create([
                'user_id' => auth()->id(),
                'name' => request('boardName'),
            ]);
        }
    }
