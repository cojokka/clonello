<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Support\Facades\DB;

    class Card extends Model
    {
        use HasFactory;

        private const INCREMENT_STEP = 84586;

        protected $guarded = [];

        public static function booted()
        {
            static::creating(static function ($model) {
                $model->position = self::query()->where('card_list_id', $model->card_list_id)->orderByDesc
                    (
                        'position'
                    )->first()?->position + self::INCREMENT_STEP;
            });

            static::saved(static function ($model) {
                if ($model->position < 0.00015) {
                    $sql = "WITH ordered AS (
                            SELECT id, :increment_step * ROW_NUMBER() OVER (ORDER BY position) AS rn
                            FROM cards
                            WHERE card_list_id = :card_list_id
                        )
                        UPDATE cards
                        SET position = ordered.rn
                        FROM ordered
                        WHERE cards.id = ordered.id;";
                    $params = ['increment_step' => self::INCREMENT_STEP, 'card_list_id' => $model->card_list_id];
                    DB::statement($sql, $params);
                }
            });
        }

        public function user(): BelongsTo
        {
            return $this->belongsTo(User::class)->select(['id', 'name', 'email']);
        }

    }
